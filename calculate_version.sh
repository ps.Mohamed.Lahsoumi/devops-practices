#!/bin/bash

last_commit_hash=$(git rev-parse --short HEAD)

last_commit_date=$(git log -1 --format=%cd --date=format:%y%m%d)

version="${last_commit_date}-${last_commit_hash}"
echo "$version"

