# Use openjdk image for running the application
FROM openjdk:8-jdk-alpine

# Create a group and user as stated in practice application will run from normal user 
RUN addgroup -S spring && adduser -S spring -G spring

# Set the working directory in the application 
WORKDIR /app

# Change to the created user
USER spring:spring

# Copy the jar file from the builder image
COPY --chown=spring:spring target/*.jar app.jar

# Expose the application's port
EXPOSE 8090

# Set environment variable for H2
ENV SPRING_PROFILES_ACTIVE=mysql

# Command to run the application
CMD ["java", "-jar", "/app/app.jar"]
